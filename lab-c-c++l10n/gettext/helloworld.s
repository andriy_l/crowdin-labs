	.file	"helloworld.c"
	.section	.rodata
.LC0:
	.string	""
	.align 8
.LC1:
	.string	"/docs/develop/Crowdin/labs/lab-c-c++l10n/gettext"
.LC2:
	.string	"helloworld"
.LC3:
	.string	"Hello World\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$.LC0, %esi
	movl	$6, %edi
	call	setlocale
	movl	$.LC1, %esi
	movl	$.LC2, %edi
	call	bindtextdomain
	movl	$.LC2, %edi
	call	textdomain
	movl	$.LC3, %edi
	call	gettext
	movq	%rax, %rdi
	movl	$0, %eax
	call	printf
	movl	$0, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.ident	"GCC: (Debian 5.3.1-7) 5.3.1 20160121"
	.section	.note.GNU-stack,"",@progbits
