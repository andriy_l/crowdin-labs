#include<libintl.h>
#include<locale.h>
#include<stdio.h>

#define _(String) gettext (String)

int main()
{
    setlocale(LC_ALL,"");
    bindtextdomain("helloworld","/docs/develop/Crowdin/labs/lab-c-c++l10n/gettext");
    textdomain("helloworld");
    printf(_("Hello World\n"));
    return 0;
}
