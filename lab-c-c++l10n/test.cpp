/**
 *
 * http://ru.cppreference.com/w/cpp/locale/LC_categories
 *
 */
#include <cstdio>
#include <clocale>
#include <ctime>
#include <cwchar>
int main()
{
    std::setlocale(LC_ALL, "en_US.UTF-8"); // the C locale will be the UTF-8 enabled English
    std::setlocale(LC_NUMERIC, "de_DE");   // decimal dot will be German
    std::setlocale(LC_TIME, "ja_JP");      // date/time formatting will be Japanese
    wchar_t str1[100];
    std::time_t t1 = std::time(NULL);
    std::wcsftime(str1, 100, L"%A %c", std::localtime(&t1));
    std::wprintf(L"Number: %.2f\nDate: %Ls\n", 3.14, str1);
    
    std::setlocale(LC_ALL, "ru_RU.UTF-8"); // the C locale will be the UTF-8 enabled English
    std::setlocale(LC_NUMERIC, "ru_RU");   // decimal dot will be German
    std::setlocale(LC_TIME, "ru_RU");      // date/time formatting will be Japanese
    wchar_t str2[100];
    std::time_t t2 = std::time(NULL);
    std::wcsftime(str2, 100, L"%A %c", std::localtime(&t2));
    std::wprintf(L"Number: %.2f\nDate: %Ls\n", 3.14, str2);
}
