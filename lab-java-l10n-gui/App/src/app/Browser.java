package app;

import javax.swing.*;
import javax.swing.event.*;
import java.net.*;
import java.io.*;
import java.awt.*;
import java.awt.event.*;
import java.beans.EventHandler;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import java.util.*;
import javax.swing.text.Document;

class Browser extends JFrame implements HyperlinkListener
{
    

    JTextPane webarea; // Region of webpage align
    String headTitle = java.util.ResourceBundle.getBundle("i18n/Browser").getString(" HELP ON OUR APP ");

    public Browser() {
        super(java.util.ResourceBundle.getBundle("i18n/Browser").getString("HELP ON OUR APP"));
        webarea = new JTextPane(); //anounce region of viewing
        webarea.setEditable(false);
        try {
// history.push(new URL(allLog.getText())); //adding page2stack
            URL url = Browser.class.getClassLoader().getResource(java.util.ResourceBundle.getBundle("i18n/Browser").getString("I18N/WELCOME.HTML"));
            webarea.setPage(url); //downloading page into the window

            setTitle(headTitle + ": "); //setting title of window
        } catch (IOException ie) {
            System.out.println(ie);
        }
        webarea.addHyperlinkListener(this);
        //webarea.addHyperlinkListener(EventHandler.create(
//      HyperlinkListener.class, webarea, "hyperLink", ""));
        JScrollPane scr = new JScrollPane(webarea);//scrolling

        getContentPane().add(scr, BorderLayout.CENTER);
        setSize(700, 400);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

    }

    public static void main(String[] args) {
        Browser b = new Browser();
    }

    @Override
    public void hyperlinkUpdate(HyperlinkEvent e) {
         HyperlinkEvent.EventType type = e.getEventType();
    final URL url = e.getURL();
    if (type == HyperlinkEvent.EventType.ENTERED) {
      System.out.println("URL: " + url);
    } else if (type == HyperlinkEvent.EventType.ACTIVATED) {
      System.out.println("Activated");

      Document doc = webarea.getDocument();
      try {
        webarea.setPage(url);
      } catch (IOException ioException) {
        System.out.println("Error following link, Invalid link");
        webarea.setDocument(doc);
      }
    }
  }
}
